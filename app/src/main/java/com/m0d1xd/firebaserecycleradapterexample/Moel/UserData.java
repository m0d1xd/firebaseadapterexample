package com.m0d1xd.firebaserecycleradapterexample.Moel;

public class UserData {
    private String userName;
    private String dateOfBirth;

    public UserData() {
    }

    public UserData(String userName, String dateOfBirth) {
        this.userName = userName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
