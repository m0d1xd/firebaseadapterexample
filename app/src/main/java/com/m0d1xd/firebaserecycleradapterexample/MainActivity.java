package com.m0d1xd.firebaserecycleradapterexample;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.m0d1xd.firebaserecycleradapterexample.Moel.UserData;
import com.m0d1xd.firebaserecycleradapterexample.ViewHolder.UserDataViewHolder;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private FirebaseRecyclerAdapter<UserData, UserDataViewHolder> adapter;
    private DatabaseReference UsersTableData;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        //init Firebase Database table for users data
        UsersTableData = firebaseDatabase.getReference("Users");


        //Init the recycler view
        recyclerView = findViewById(R.id.rv_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


//     for searching in adapter for specific user   @param text can be the user uid
//        Query query = UsersTableData.orderByChild("userUid").equalTo(text);

        Query query = UsersTableData;

        FirebaseRecyclerOptions<UserData> userDataFirebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<UserData>()
                .setQuery(query, UserData.class)
                .build();

        //Creating the adapter
        adapter = new FirebaseRecyclerAdapter<UserData, UserDataViewHolder>(userDataFirebaseRecyclerOptions) {
            @Override
            protected void onBindViewHolder(@NonNull UserDataViewHolder holder, final int position, @NonNull UserData model) {
                holder.userName.setText(model.getUserName());
                holder.userDOB.setText(model.getDateOfBirth());
                holder.iv_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseReference delete = FirebaseDatabase.getInstance().getReference("Users").child(adapter.getRef(position).getKey());
                        delete.removeValue();
                    }
                });
            }

            @NonNull
            @Override
            public UserDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.rv_list, viewGroup, false);

                return new UserDataViewHolder(itemView);
            }
        };

        //setting the adapter
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        adapter.notifyDataSetChanged();
        final EditText et_name = findViewById(R.id.et_name);
        final EditText et_dob = findViewById(R.id.et_dob);
        Button add = findViewById(R.id.btn_add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = et_name.getText().toString();
                UserData user = new UserData();
                if (TextUtils.isEmpty(name)) {
                    return;
                }
                String dob = et_dob.getText().toString();
                if (TextUtils.isEmpty(dob)) {
                    return;
                }
                user.setUserName(name);
                user.setDateOfBirth(dob);
                UsersTableData.push().setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(MainActivity.this, "User Added", Toast.LENGTH_SHORT).show();
                        et_dob.setText("");
                        et_name.setText("");
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseApp.initializeApp(this);
    }
}
