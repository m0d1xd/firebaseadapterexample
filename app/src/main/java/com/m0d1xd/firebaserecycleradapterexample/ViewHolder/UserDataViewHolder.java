package com.m0d1xd.firebaserecycleradapterexample.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.m0d1xd.firebaserecycleradapterexample.R;

public class UserDataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView userName, userDOB;
    public ImageView iv_delete;

    public UserDataViewHolder(@NonNull View itemView) {
        super(itemView);
        userName = itemView.findViewById(R.id.tv_username);
        userDOB = itemView.findViewById(R.id.tv_dob);
        iv_delete = itemView.findViewById(R.id.iv_delete);
        iv_delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}
